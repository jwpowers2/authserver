package main

import (
	"net/http"

	"bitbucket.org/jwpowers2/authserver/api"
	"github.com/rs/cors"
)

func main() {
	srv := api.NewServer()
	c := cors.New(cors.Options{
		AllowedOrigins:   []string{"http://172.16.1.80", "http://172.16.1.80:8080"},
		AllowedHeaders:   []string{"Origin", "X-Requested-With", "Content-Type", "Accept", "Authorization"},
		AllowCredentials: true,
		AllowedMethods:   []string{"GET", "PUT", "POST", "DELETE", "OPTIONS"},
		Debug:            true,
	})
	handler := cors.Default().Handler(srv.Router)
	handler = c.Handler(handler)

	http.ListenAndServe(":8080", handler)
}
