module bitbucket.org/jwpowers2/authserver

go 1.12

require (
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/rs/cors v1.8.2
	github.com/xdg-go/stringprep v1.0.3 // indirect
	go.mongodb.org/mongo-driver v1.8.3
	golang.org/x/crypto v0.0.0-20220507011949-2cf3adece122
)
