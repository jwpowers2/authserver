package controllers

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"bitbucket.org/jwpowers2/authserver/api/connections"
	"bitbucket.org/jwpowers2/authserver/api/models"
	"bitbucket.org/jwpowers2/authserver/auth"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type ResponseMessage struct {
	Message string
	Success bool
	Count   int64
}

type UserController struct {
	Valid_jwts map[string]string
}

func NewUserController() *UserController {
	return &UserController{}
}

func (c UserController) CreateUser() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		var user models.User
		defer cancel()
		if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		// create password hash here
		m := ResponseMessage{Success: true}
		PasswordHash := auth.HashPassword(user.Password)
		result, err := connections.Database.Collection("users").InsertOne(ctx, bson.D{
			{Key: "UpdatedAt", Value: time.Now()},
			{Key: "CreatedAt", Value: time.Now()},
			{Key: "LastName", Value: user.LastName},
			{Key: "FirstName", Value: user.FirstName},
			{Key: "UserName", Value: user.UserName},
			{Key: "Password", Value: PasswordHash},
			{Key: "Role", Value: user.Role},
		})
		if err != nil {
			fmt.Println(result)
			m.Success = false
		}
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Origin", "http://172.16.1.80")
		if err := json.NewEncoder(w).Encode(m); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		return
	}
}

func (c UserController) ListUsers() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		jwt_cookie, err := r.Cookie("jwt")
		if err != nil {
			fmt.Println(jwt_cookie)
			return
		}
		jwt_cookie_value := jwt_cookie.Value
		found := auth.FindJwt(c.Valid_jwts, jwt_cookie_value)

		if found == false {
			return
		}
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()
		var users []models.User
		users_coll := connections.Database.Collection("users")
		filter := bson.D{}
		sort := bson.D{{"UserName", 1}}
		opts := options.Find().SetSort(sort)
		cur, err := users_coll.Find(ctx, filter, opts)
		if err != nil {
			panic(err)
		}
		if err = cur.All(context.TODO(), &users); err != nil {
			panic(err)
		}
		defer cur.Close(ctx)
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Origin", "http://172.16.1.80")
		if err := json.NewEncoder(w).Encode(users); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		return
	}
}

func (c UserController) DeleteUser() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()
		//idStr, _ := mux.Vars(r)["id"]
		idStr := r.FormValue("id")
		obId, _ := primitive.ObjectIDFromHex(idStr)
		result, err := connections.Database.Collection("users").DeleteOne(ctx, bson.M{"_id": obId})
		if err != nil {
			fmt.Println(err)
		}
		m := ResponseMessage{Success: true, Count: result.DeletedCount}
		w.Header().Set("Access-Control-Allow-Origin", "http://172.16.1.80")
		if err := json.NewEncoder(w).Encode(m); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		return
	}
}

func (c UserController) Login() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		var user models.User
		defer cancel()
		if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		var user_db models.User
		err := connections.Database.Collection("users").FindOne(ctx, bson.M{"UserName": &user.UserName}).Decode(&user_db)
		if err != nil {
			fmt.Println(err)
		}

		check, msg := auth.VerifyPassword(user_db.Password, user.Password)
		if check == false {
			fmt.Println(msg)
		}
		user_db.Password = ""
		jwt := auth.CreateJwt(user_db.UserName, user_db.Role)
		c.Valid_jwts = auth.InsertJwt(c.Valid_jwts, jwt)
		cookie := &http.Cookie{
			Name:     "jwt",
			Value:    jwt,
			MaxAge:   6000,
			Secure:   false,
			HttpOnly: true,
		}
		http.SetCookie(w, cookie)
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Origin", "http://172.16.1.80")
		if err := json.NewEncoder(w).Encode(user_db); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		return

	}
}

func (c UserController) Logout() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		jwt_cookie, err := r.Cookie("jwt")
		if err != nil {
			fmt.Println(jwt_cookie)
			return
		}
		jwt_cookie_value := jwt_cookie.Value
		found := auth.FindJwt(c.Valid_jwts, jwt_cookie_value)
		m := ResponseMessage{Success: false}
		if found == true {
			// jwt is present and valid, delete and logout
			c.Valid_jwts = auth.DeleteJwt(c.Valid_jwts, jwt_cookie_value)
			fmt.Println(c.Valid_jwts)
			m.Success = true
		}
		fmt.Println(m)
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Origin", "http://172.16.1.80")
		if err := json.NewEncoder(w).Encode(m); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		return
	}
}
