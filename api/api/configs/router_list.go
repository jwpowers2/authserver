package configs

import (
	"net/http"

	"bitbucket.org/jwpowers2/authserver/api/controllers"
)

type Route struct {
	RouteName        string
	ControllerMethod func() http.HandlerFunc
	HttpMethod       string
}

func NewRouteList() []Route {
	// init controller object, will have jwt_map in it
	userController := controllers.NewUserController()
	dataController := controllers.NewDataController()
	userController.Valid_jwts = make(map[string]string)
	RouteList := []Route{
		Route{
			RouteName:        "/users",
			ControllerMethod: userController.ListUsers,
			HttpMethod:       "GET",
		},
		Route{
			RouteName:        "/users",
			ControllerMethod: userController.CreateUser,
			HttpMethod:       "POST",
		},
		Route{
			RouteName:        "/users/{id}",
			ControllerMethod: userController.DeleteUser,
			HttpMethod:       "DELETE",
		},
		Route{
			RouteName:        "/users/login",
			ControllerMethod: userController.Login,
			HttpMethod:       "POST",
		},
		Route{
			RouteName:        "/users/logout",
			ControllerMethod: userController.Logout,
			HttpMethod:       "GET",
		},
		Route{
			RouteName:        "/ws",
			ControllerMethod: dataController.Upgrade,
			HttpMethod:       "GET",
		},
	}
	return RouteList
}
