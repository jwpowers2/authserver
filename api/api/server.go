package api

import (
	"bitbucket.org/jwpowers2/authserver/api/configs"
	"github.com/gorilla/mux"
)

type Server struct {
	*mux.Router
}

func NewServer() Server {
	s := Server{
		Router: mux.NewRouter(),
	}
	s.routes()
	return s
}

func (s *Server) routes() {

	RouteList := configs.NewRouteList()
	for _, item := range RouteList {
		s.HandleFunc(item.RouteName, item.ControllerMethod()).Methods(item.HttpMethod)
	}
}
