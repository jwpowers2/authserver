package auth

import (
	"time"

	"github.com/golang-jwt/jwt"
)

func CreateJwt(userName string, userRole string) string {
	var hmacSampleSecret = []byte("hellothere")

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"user":    userName,
		"role":    userRole,
		"expTime": "8h",
		"nbf":     time.Date(2015, 10, 10, 12, 0, 0, 0, time.UTC).Unix(),
	})
	tokenString, err := token.SignedString(hmacSampleSecret)
	if err == nil {
		return tokenString
	} else {
		return "there was an error"
	}

}
