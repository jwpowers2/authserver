package auth

func InsertJwt(jwtMap map[string]string, jwt string) map[string]string {
	// insert into map return bool
	jwtMap[jwt] = ""
	return jwtMap
}

func FindJwt(jwtMap map[string]string, jwt string) bool {
	// find in map and return bool
	_, found := jwtMap[jwt]
	return found
}

func DeleteJwt(jwtMap map[string]string, jwt string) map[string]string {
	// remove from map, return bool
	delete(jwtMap, jwt)
	return jwtMap
}
