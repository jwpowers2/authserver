import React from "react";
import { Redirect } from "react-router-dom";
import CssBaseline from "@mui/material/CssBaseline";
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import NavBar from "../component/NavBar";
import axios from "axios";

axios.defaults.withCredentials = true;

class Login extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
    };
  }
  handleChange = e => {
    console.log(e.target.value);
    this.setState({ [e.target.name]: e.target.value });
  }
  submitUser = () => {
    let payload = {
      username: this.state.username,
      password: this.state.password
    };

    let userQuery = `http://${process.env.REACT_APP_SERVER_ADDRESS}/users/login`;
    axios
      .post(userQuery, payload, {
        Headers: {'Content-Type': 'application/json'},
        credentials: 'include'
        
      })
      .then(d => {
        this.setState({ username: "" });
        this.setState({ password: "" });
       
        this.props.history.push("/dashboard");
        
      })
      .catch(e => {
        //this.message = e;
      });
  };
  render() {
    return (
      <>
        <CssBaseline />

        <NavBar />
        <Container maxWidth="lg">
          <Box
            component="main"
            sx={{ flexGrow: 1, p: 3, marginTop: "3em", marginLeft: "300px" }}
          >
            <Grid container spacing={2}>
              <Grid item xs={8}>
                <Box component="span" sx={{ textAlign: "center" }}>
                  <h1>Login</h1>
                </Box>
                <Box
                  component="form"
                  sx={{
                    "& > :not(style)": { m: 1, width: "50ch" },
                    textAlign: "center"
                  }}
                  noValidate
                  autoComplete="off"
                >
                  <TextField
                    id="outlined-basic"
                    label="username"
                    variant="outlined"
                    name="username"
                    value={this.state.username}
                    onChange={this.handleChange}
                  />
                  <TextField
                    id="outlined-password-input"
                    label="password"
                    variant="outlined"
                    name="password"
                    type="password"
                    value={this.state.password}
                    onChange={this.handleChange}
                  />
                  <br></br>
                  <Button
                    sx={{ color: "white", backgroundColor: "black" }}
                    variant="contained"
                    onClick={this.submitUser}
                  >
                    Submit
                  </Button>
                </Box>
              </Grid>
            </Grid>
          </Box>
        </Container>
      </>
    );
  }
}

export default Login;
