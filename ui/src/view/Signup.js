import React from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import NavBar from "../component/NavBar";
import axios from "axios";

axios.defaults.withCredentials = true;

class Signup extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      confirmpassword: "",
      message: ""
    };
  }
  handleChange = e => {
    console.log(e.target.value);
    this.setState({ [e.target.name]: e.target.value });
  };
  submitUser = () => {
    if (this.state.password !== this.state.confirmpassword) return;
    let payload = {
      email: this.state.email,
      password: this.state.password
    };
    let uri = `http://${process.env.REACT_APP_SERVER_ADDRESS}/api/users/register/`;
    axios
      .post(uri, payload, {Headers: {'Content-Type': 'application/json'}, withCredentials: true})
      .then(d => {
        this.setState({ email: "" });
        this.setState({ password: "" });
        this.setState({ confirmpassword: "" });

        if (d.data.token) {
          localStorage.setItem("jwt", d.data.token);
          this.props.history.push("/dashboard");
        }
      })
      .catch(e => {
        this.setState({ message: e });
      });
  };
  render() {
    return (
      <>
        <CssBaseline />

        <NavBar />
        <Container maxWidth="lg">
          <Box
            component="main"
            sx={{ flexGrow: 1, p: 3, marginTop: "3em", marginLeft: "300px" }}
          >
            <Grid container spacing={2}>
              <Grid item xs={8}>
                <Box component="span" sx={{ textAlign: "center" }}>
                  <h1>Local Auth Sign Up</h1>
                </Box>
                <Box
                  component="form"
                  sx={{
                    "& > :not(style)": { m: 1, width: "50ch" }
                  }}
                  noValidate
                  autoComplete="off"
                >
                  <TextField
                    id="outlined-basic"
                    label="email"
                    variant="outlined"
                    name="email"
                    value={this.state.email}
                    onChange={this.handleChange}
                  />
                </Box>
                <Box
                  component="form"
                  sx={{
                    "& > :not(style)": { m: 1, width: "50ch" }
                  }}
                  noValidate
                  autoComplete="off"
                >
                  <TextField
                    id="outlined-password-input"
                    label="new password"
                    variant="outlined"
                    name="password"
                    type="password"
                    value={this.state.password}
                    onChange={this.handleChange}
                  />
                </Box>
                <Box
                  component="form"
                  sx={{
                    "& > :not(style)": { m: 1, width: "50ch" }
                  }}
                  noValidate
                  autoComplete="off"
                >
                  <TextField
                    id="outlined-password-input"
                    label="confirm password"
                    variant="outlined"
                    name="confirmpassword"
                    type="password"
                    value={this.state.confirmpassword}
                    onChange={this.handleChange}
                  />
                  <br></br>

                  <Button
                    sx={{ color: "white", backgroundColor: "black" }}
                    variant="contained"
                    onClick={this.submitUser}
                  >
                    Submit
                  </Button>
                  <p>{this.state.message}</p>
                </Box>
              </Grid>
            </Grid>
          </Box>
        </Container>
      </>
    );
  }
}

export default Signup;
