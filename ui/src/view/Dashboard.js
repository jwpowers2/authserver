import React from "react";
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import Link from "@mui/material/Link";
import Drawer from "@mui/material/Drawer";
import List from "@mui/material/List";
import Toolbar from "@mui/material/Toolbar";
import Divider from "@mui/material/Divider";
import CssBaseline from "@mui/material/CssBaseline";
import NavBar from "../component/NavBar";
import axios from "axios";

class Dashboard extends React.PureComponent {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <>
        <Box sx={{ display: "flex" }}>
          <CssBaseline />

          <NavBar />
          <Drawer
            variant="permanent"
            sx={{
              width: 300,
              flexShrink: 0,
              [`& .MuiDrawer-paper`]: { width: 300, boxSizing: "border-box" }
            }}
          >
            <Toolbar />
            <Box sx={{ overflow: "auto", textAlign: "center" }}>
              <List>
                <Link href="/dashboard/users" underline="none">
                  <h2>useradmin</h2>
                </Link>
              </List>
              <Divider />
            </Box>
          </Drawer>
          <Container maxWidth="lg"></Container>
        </Box>
      </>
    );
  }
}

export default Dashboard;
