import React from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import NavBar from "../component/NavBar";

class Welcome extends React.PureComponent {
  render() {
    const text1 =
      "Testing Env for creating a golang backend";
    const text2 =
      "hurray";

    return (
      <>
        <CssBaseline />

        <NavBar />
        <Container maxWidth="m" sx={{ margin: "auto", display: "flex" }}>
          <Box component="main" margin="auto">
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <Box component="span" sx={{ textAlign: "center" }}>
                  <h1>radius and web_server</h1>

                  <Card sx={{ maxWidth: 500, fontSize: "500px" }}>
                    <CardContent>
                      <Typography variant="body2" color="text.secondary">
                        <h2>{text1}</h2>
                      </Typography>
                    </CardContent>
                  </Card>
                  <br></br>
                  <Card sx={{ maxWidth: 500 }}>
                    <CardContent>
                      <Typography variant="body2" color="text.secondary">
                        <h2>{text2}</h2>
                      </Typography>
                    </CardContent>
                  </Card>
                  <br></br>
                </Box>
              </Grid>
            </Grid>
          </Box>
        </Container>
      </>
    );
  }
}

export default Welcome;
