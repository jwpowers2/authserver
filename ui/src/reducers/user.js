const user = (
    state = {
      selectedUsers: [],
      userCount: 0,
    },
    action
  ) => {
    switch (action.type) {
      case "SET_SELECTED_USERS":
        return { ...state, selectedUsers: action.name };
      case "SET_USER_COUNT":
        return { ...state, userCount: action.number };
      default:
        return state;
    }
  };
export default user;