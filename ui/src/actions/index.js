export const setSelectedUsers = name => {
    return {
      type: "SET_SELECTED_USERS",
      name
    };
};
export const setUserCount = number => {
  return {
    type: "SET_USER_COUNT",
    number
  };
};