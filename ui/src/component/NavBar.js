import React from "react";
import { withRouter } from "react-router";
import Link from "@mui/material/Link";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import CssBaseline from "@mui/material/CssBaseline";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import axios from 'axios';

class NavBar extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      navigate: false
    };
  }
  logout(){
    // send request to backend to logout, clears jwt from valid jwt's AND clears jwt cookie and it gets TRUE back, do redirect
    let userQuery = `http://${process.env.REACT_APP_SERVER_ADDRESS}/users/logout`;
    axios
      .get(userQuery, {withCredentials: true})
      .then(d => {
        if (d.data.Success === true) {
          
          console.log("You've been logged out on the backend");
          this.props.history.push('/')
        }
      })
      .catch(e => {
        console.log(e);
      });
    
  }
  render() {
    
    return (
      <>
        <CssBaseline />
        <AppBar
          position="absolute"
          sx={{
            zIndex: theme => theme.zIndex.drawer + 10,
            backgroundColor: "black",
            color: "white",
            borderBottom: "1px solid white"
          }}
        >
          <Toolbar>
            <Container sx={{ textAlign: "left" }}>
              <Typography variant="h2" color="inherit">
                <Link sx={{ color: "white" }} href="/" underline="none">
                  testing
                </Link>
              </Typography>
            </Container>
            <Container sx={{ textAlign: "right" }}>
              <Link
                sx={{ color: "white", fontSize: "20px", marginLeft: "1em" }}
                underline="none"
                href="/dashboard"
              >
                dashboard
              </Link>
              <Link
                sx={{ color: "white", fontSize: "20px", marginLeft: "1em" }}
                underline="none"
                href="/signup"
              >
                sign up
              </Link>
              <Link
                sx={{ color: "white", fontSize: "20px", marginLeft: "1em" }}
                underline="none"
                href="/login"
              >
                login
              </Link>
              <Link
                sx={{ color: "white", fontSize: "20px", marginLeft: "1em" }}
                underline="none"
                onClick={() => this.logout()}
              >
                logout
              </Link>
            </Container>
          </Toolbar>
        </AppBar>
      </>
    );
  }
}

export default withRouter(NavBar);
