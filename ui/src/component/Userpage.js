import React from "react";
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import TextField from "@mui/material/TextField";
import Userlist from "./Userlist";
import UserPanel from "./UserPanel";

class Userpage extends React.PureComponent {
  render() {
    return (
      <>
        <Container maxWidth="lg">
          <Box
            component="main"
            sx={{ flexGrow: 1, p: 3, marginTop: "3em", marginLeft: "300px" }}
          >
            <Grid container spacing={2}>
              <Grid item xs={4}>
                <h1>User List</h1>
                <Userlist />
              </Grid>
              <Grid item xs={4}>
                <h1>User Panel</h1>
                <UserPanel />
              </Grid>
            </Grid>
          </Box>
        </Container>
      </>
    );
  }
}

export default Userpage;
