import React from "react";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import Checkbox from "@mui/material/Checkbox";
import { connect } from "react-redux";
import axios from "axios";
import { setSelectedUsers, setUserCount } from "../actions";
// use a test list from here
// as a user from list is checked, it is added to store delete list
axios.defaults.withCredentials = true;

class Userlist extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      userList: [],
      selectedUsers: [],
      userCount: 0
    };
    this.handleChange = this.handleChange.bind(this);
  }
  getUsers(){
    let userQuery = `http://${process.env.REACT_APP_SERVER_ADDRESS}/users`;
    axios
      .get(userQuery, {withCredentials: true})
      .then(d => {
        //this.props.setUserCount(d.data.length)
        this.setState({ userList: d.data });
        console.log(d);
      })
      .catch(e => {
        console.log(e);
      });
  }
  componentDidMount() {
    let userQuery = `http://${process.env.REACT_APP_SERVER_ADDRESS}/users`;
    axios
      .get(userQuery, {withCredentials: true})
      .then(d => {
        this.props.setUserCount(d.data.length)
        this.setState({ userList: d.data });
        console.log(d);
      })
      .catch(e => {
        console.log(e);
      });
  }
  componentDidUpdate(prevProps){
    console.log(prevProps.userCount)
    console.log(this.props.userCount)
    if (prevProps.userCount !== this.props.userCount){
      console.log("LENGTHS ARE DIFF")
      console.log(`PREv PROPS user count ${prevProps.userCount} and props user count ${this.props.userCount}`)
      this.getUsers()
    }
  }
  handleChange(event) {
    let newList = [];
    let oldList = this.state.selectedUsers;
    if (event.target.checked === true) {
      oldList.push(event.target.value);
      newList = oldList;
    } else if (event.target.checked === false) {
      newList = oldList.filter(id => {
        return id !== event.target.value;
      });
    }
    this.setState({ selectedUsers: newList });
    this.props.setSelectedUsers(newList);
    console.log(this.state.selectedUsers);
    console.log(event.target.checked);
  }
  render() {
    let showUserList = <></>
    if (this.state.userList.length > 0){
      showUserList = this.state.userList.map((item, index) => {
        return (
          <ListItem key={index} value={item._id}>
            <ListItemButton role={undefined} dense>
              <ListItemIcon>
                <Checkbox
                  edge="start"
                  onChange={this.handleChange}
                  tabIndex={-1}
                  disableRipple
                  value={item._id}
                />
              </ListItemIcon>
              <ListItemText id={item._id} primary={item.username} />
            </ListItemButton>
          </ListItem>
        );
      }, this);
    }
    return (
      <>
        <List>{showUserList}</List>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    selectedUsers: state.selectedUsers,
    userCount: state.userCount
  };
};
const mapDispatchToProps = () => {
  return {
    setSelectedUsers,
    setUserCount,
  };
};

export default connect(mapStateToProps, mapDispatchToProps())(Userlist);
