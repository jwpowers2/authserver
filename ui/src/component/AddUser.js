import React from "react";
import { connect } from "react-redux";
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";
import { setUserCount } from "../actions";
import axios from "axios";

axios.defaults.withCredentials = true;

class AddUser extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      confirmpassword: "",
      role: "user"
    };
  }
  handleChange = e => {
    console.log(e.target.value);
    this.setState({ [e.target.name]: e.target.value });
  };
  submitUser = () => {
    let payload;
    if (this.state.password === this.state.confirmpassword) {
      payload = {
        username: this.state.username,
        password: this.state.password,
        role: this.state.role
      };
    }
    let userQuery = `http://${process.env.REACT_APP_SERVER_ADDRESS}/users`;
    axios
      .post(userQuery, payload, {
        headers: {'Content-Type': "application/json"},
        withCredentials: true
      })
      .then(d => {
        this.setState({ username: "" });
        this.setState({ password: "" });
        this.setState({ confirmpassword: "" });
        this.setState({ role: "user" });
        this.props.setUserCount(this.props.userCount++)
        
      })
      .catch(e => {
        //this.message = e;
      });
  };
  render() {
    return (
      <>
        <Container maxWidth="lg">
          <Box component="main" sx={{ flexGrow: 1, p: 3, marginTop: "3em" }}>
            <Grid container spacing={2}>
              <h4>add user</h4>
              <Grid item xs={8}>
                <Box
                  component="form"
                  sx={{
                    "& > :not(style)": { m: 1, width: "50ch" }
                  }}
                  noValidate
                  autoComplete="off"
                >
                  <TextField
                    id="outlined-basic"
                    label="username"
                    variant="outlined"
                    name="username"
                    value={this.state.username}
                    onChange={this.handleChange}
                  />
                </Box>
                <Box
                  component="form"
                  sx={{
                    "& > :not(style)": { m: 1, width: "50ch" }
                  }}
                  noValidate
                  autoComplete="off"
                >
                  <FormControl component="fieldset">
                    <FormLabel component="legend"></FormLabel>
                    <RadioGroup
                      row
                      aria-label="role"
                      name="role"
                      onChange={this.handleChange}
                    >
                      <FormControlLabel
                        value="client"
                        control={<Radio />}
                        label="Client"
                      />
                      <FormControlLabel
                        value="user"
                        control={<Radio />}
                        label="User"
                      />
                      <FormControlLabel
                        value="admin"
                        control={<Radio />}
                        label="Admin"
                      />
                    </RadioGroup>
                  </FormControl>
                </Box>
                <Box
                  component="form"
                  sx={{
                    "& > :not(style)": { m: 1, width: "50ch" }
                  }}
                  noValidate
                  autoComplete="off"
                >
                  <TextField
                    id="outlined-password-input"
                    label="password"
                    variant="outlined"
                    name="password"
                    type="password"
                    value={this.state.password}
                    onChange={this.handleChange}
                  />
                  <br></br>
                  <TextField
                    id="outlined-password-input"
                    label="confirm password"
                    variant="outlined"
                    name="confirmpassword"
                    type="password"
                    value={this.state.confirmpassword}
                    onChange={this.handleChange}
                  />
                  <br></br>
                  <Button variant="contained" onClick={this.submitUser}>
                    Submit
                  </Button>
                </Box>
              </Grid>
            </Grid>
          </Box>
        </Container>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    userCount: state.userCount
  };
};
const mapDispatchToProps = () => {
  return {
    //setSelectedUsers,
    setUserCount,
  };
};

export default connect(mapStateToProps, mapDispatchToProps())(AddUser);
