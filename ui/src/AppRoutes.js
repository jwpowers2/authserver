import React, { Component } from "react";
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";
import Dashboard from "./view/Dashboard";
import Userpage from "./component/Userpage";
import Login from "./view/Login";
import Signup from "./view/Signup";
import Welcome from "./view/Welcome";

class AppRoutes extends Component {
  render() {
    return (
      <Router>
        <Route
          path="/"
          render={({ match: { url } }) => (
            <>
              <Route path="/" exact component={Welcome} />
              <Route path="/signup" component={Signup} />
              <Route path="/login" component={Login} />
              <Route
                path="/dashboard"
                render={({ match: { url } }) => (
                  <>
                    <Route path={`${url}/`} component={Dashboard} />
                    <Route path={`${url}/users`} component={Userpage} />
                  </>
                )}
              />
            </>
          )}
        />
        <Route path="*" component={Welcome}/>
      </Router>
    );
  }
}

export default AppRoutes;
